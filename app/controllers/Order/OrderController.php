<?php
defined('BASEPATH') or exit('No direct access');
require_once ROOT . "/app/models/Order/OrderModel.php";

class OrderController extends Controller
{
  private $model;

  public function __construct()
  {
    $this->model = new OrderModel();
  }

  public function exec()
  {
    $this->orderList();
  }

  public function orderList($message = "", $message_type = "success")
  {
    $orders = $this->model->orderList();

    $params = [

      "orders" => $orders,
      'message' => $message,
      "show_order_list" => true,
    ];

    return $this->render(__CLASS__, $params);
  }

  public function addOrder()
  {
    $products   = $this->model->orderProducts();
    $providers  = $this->model->orderProviders();

    $params = [
      "form_order" => true,
      'products' => $products,
      'providers' => $providers,
    ];

    return $this->render(__CLASS__, $params);
  }

  public function createOrder($request_params)
  {
    $order = $this->model->createOrder($request_params);

    return $this->orderList('Order Create');
  }

  public function show($request_params)
  {
    $orderDetails = $this->model->show($request_params);

    $params = [
      'order_detail' => true,
      'orderDetails' => $orderDetails,
    ];

    return $this->render(__CLASS__, $params);
  }
}
