<?php
defined('BASEPATH') or exit('No direct access');
require_once ROOT . "/app/models/Provider/ProviderModel.php";

class ProviderController extends Controller
{
  private $model;

  public function __construct()
  {
    $this->model = new ProviderModel();
  }

  public function exec()
  {
    $this->providerList();
  }

  public function providerList($message = "", $message_type = "success")
  {
    $providers = $this->model->providersList();

    $params = [

      "providers" => $providers,
      'message' => $message
    ];

    return $this->render(__CLASS__, $params);
  }

  public function addProvider($request_params)
  {
    $result = $this->model->addProvider($request_params);
    return $this->providerList('Has been Provider ADD');
  }
}
