<?php
defined('BASEPATH') or exit('No direct access');
require_once ROOT . "/app/models/Product/ProductModel.php";

class ProductController extends Controller
{
  private $model;

  public function __construct()
  {
    $this->model = new ProductModel();
  }

  public function exec()
  {
    $this->productsList();
  }

  public function productsList($message = "", $message_type = "success")
  {
    $products = $this->model->productsList();

    $params = [

      "products" => $products,
      'message' => $message
    ];

    return $this->render(__CLASS__, $params);
  }

  public function dataAjax()
  {
    $products = $this->model->dataAjax();

    return $products;
  }

  public function addProduct($request_params)
  {
    $result = $this->model->addProduct($request_params);

    if (!$result) {
      die("Execute query error, because: " . print_r($this->db->errorInfo(), true));
    } else {
      header("Location: /product/message=Has been Added");
      $this->productsList('Has been Added');
    }
  }
}
