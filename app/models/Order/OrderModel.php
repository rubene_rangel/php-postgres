<?php
defined('BASEPATH') or exit('No direct access');
require_once ROOT . "/app/models/Product/ProductModel.php";
require_once ROOT . "/app/models/Provider/ProviderModel.php";

/**
 * Model to table Orders
 */
class OrderModel extends Model
{
  public function __contruct()
  {
    parent::__construct();
  }

  public function orderList()
  {
    $sql = "SELECT * FROM get_orders();";
    $stmt = $this->db->query($sql);

    if (!$stmt)
      die("Execute query error, because: " . print_r($this->db->errorInfo(), true));

    $orders = [];

    while ($row = $stmt->fetch()) {
      $orders[] = [
        'id' => $row['id'],
        'total' => $row['total'],
        'created_at' => $row['created_at'],
      ];
    }

    return $orders;
  }

  public function orderProducts()
  {
    $products = new ProductModel;

    $listProducts = $products->productsList();

    return $listProducts;
  }

  public function orderProviders()
  {
    $provider = new ProviderModel;

    $listProviders = $provider->providersList();

    return $listProviders;
  }

  public function createOrder($request_params)
  {
    $totalOrder = floatval($request_params["total"]);
    $totalRows = intval($request_params["totalRows"]);

    $sqlInsertOrder = "INSERT INTO orders (total) VALUES (:total) RETURNING id";

    $stmt = $this->db->prepare($sqlInsertOrder);
    $stmt->bindParam(":total", $totalOrder);
    $stmt->execute();

    $idOrder = $stmt->fetch(PDO::FETCH_ASSOC);
    $idOrder = $idOrder['id'];

    for ($i = 0; $i < $totalRows; $i++) {
      $product = $request_params["product"][$i];

      $provider = $request_params["providers"][$i];

      $price = $request_params["price"][$i];

      $quantity = $request_params["quantity"][$i];

      $sqlInsertDetailOrder = "INSERT
        INTO
          order_details(id_order, id_product, id_provider, price, quantity)
        VALUES(:id_order, :id_product, :id_provider, :price, :quantity)";

      $stmtDt = $this->db->prepare($sqlInsertDetailOrder);
      $stmtDt->bindParam(":id_order", $idOrder);
      $stmtDt->bindParam(":id_product", $product);
      $stmtDt->bindParam(":id_provider", $provider);
      $stmtDt->bindParam(":price", $price);
      $stmtDt->bindParam(":quantity", $quantity);
      $stmtDt->execute();
    }

    return 1;
  }

  public function show($request_params)
  {
    $orderId = $request_params;

    $sql = "SELECT * FROM get_order_details($orderId)";
    $stmt = $this->db->query($sql);

    $orderDetails = [];

    while ($row = $stmt->fetch()) {
      $orderDetails[] = [
        'id_order' => $row['id_order'],
        'name_product' => $row['name_product'],
        'name_provider' => $row['name_provider'],
        'price' => $row['price'],
        'quantity' => $row['quantity'],
      ];
    }
    return $orderDetails;
  }
}
