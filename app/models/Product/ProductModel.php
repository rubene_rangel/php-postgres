<?php
defined('BASEPATH') or exit('No direct access');

/**
 * Model to table products
 */
class ProductModel extends Model
{
  public function __contruct()
  {
    parent::__construct();
  }

  public function productsList()
  {
    $sql = "SELECT * FROM get_products();";
    $stmt = $this->db->query($sql);

    $products = [];

    while ($row = $stmt->fetch()) {
      $products[] = [
        'id' => $row['id'],
        'name' => $row['name'],
        'price' => $row['price'],
      ];
    }

    return $products;
  }

  public function dataAjax()
  {
    $sql = "SELECT * FROM get_products();";
    $stmt = $this->db->query($sql);

    $data = [];

    while ($row = $stmt->fetch()) {
      $data['products'][] = [
        'id' => $row['id'],
        'name' => $row['name'],
        'price' => $row['price'],
      ];
    }

    $sql = "SELECT * FROM get_providers();";
    $stmt = $this->db->query($sql);

    while ($row = $stmt->fetch()) {
      $data['providers'][] = [
        'id' => $row['id'],
        'name' => $row['name'],
      ];
    }

    echo json_encode($data);
  }

  public function addProduct($params)
  {
    $sql    = "INSERT INTO products(name, price) VALUES (:name, :price);";

    $stmt   = $this->db->prepare($sql);

    $stmt->bindValue(':name', $params['name'], PDO::PARAM_STR);
    $stmt->bindValue(':price', $params['price'], PDO::PARAM_STR);
    $stmt->execute();

    return $stmt;
  }
}
