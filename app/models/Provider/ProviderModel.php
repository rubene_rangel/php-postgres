<?php
defined('BASEPATH') or exit('No direct access');

/**
 * Model to table providers
 */
class ProviderModel extends Model
{
  public function __contruct()
  {
    parent::__construct();
  }

  public function providersList()
  {
    $sql = "SELECT * FROM get_providers();";
    $stmt = $this->db->query($sql);

    $providers = [];

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $providers[] = [
        'id' => $row['id'],
        'name' => $row['name'],
      ];
    }

    return $providers;
  }

  public function addProvider($params)
  {
    $sql    = "INSERT INTO providers(name) VALUES (:name)";

    $stmt   = $this->db->prepare($sql);

    $stmt->bindValue(':name', $params['name']);

    return $stmt->execute();
  }
}
