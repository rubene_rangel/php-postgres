<div class="col-12 text-center">
  <h2>Order</h2>
</div>
<div class="col-12">
  <div class="row border py-3">
    <div class="col-12 text-center">
      <h4>Discount Factor</h4>
    </div>
    <div class="col-6">
      <h5>Percentage %</h5>
      <input type="text" name="percentage" id="percentage" class="form-control text-right percentage">
    </div>
    <div class="col-6">
      <h5>From:</h5>
      <input type="text" name="quantityFrom" id="quantityFrom" class="form-control text-right quantityFrom">
    </div>
  </div>
  <form action="/order/createOrder" method="POST" autocomplete="off">
    <table class="table" id="order-table">
      <input type="hidden" name="totalRows" id="totalRows" value="1">
      <thead>
        <tr>
          <th>Product</th>
          <th>Provider</th>
          <th>Price</th>
          <th>Quantity</th>
          <th>Total</th>

        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <select name="product[]" id="product_row1" class="form-control" data-srno="1" required>
              <option value="">Select</option>
              <?php foreach ($products as $product) : ?>
                <option value="<?php echo $product['id'] ?>"><?php echo $product['name'] ?></option>
              <?php endforeach; ?>
            </select>
          </td>
          <td>
            <select name="providers[]" id="providers_row1" class="form-control" data-srno="1" required>
              <option value="">Select</option>
              <?php foreach ($providers as $provider) : ?>
                <option value="<?php echo $provider['id'] ?>"><?php echo $provider['name'] ?></option>
              <?php endforeach; ?>
            </select>
          </td>
          <td>
            <input type="text" name="price[]" id="price_row1" data-srno="1" class="form-control text-right price" required>
          </td>
          <td>
            <input type="number" min="1" name="quantity[]" id="quantity_row1" data-srno="1" class="form-control text-right quantity" required>
          </td>
          <td>
            <input type="text" min="1" name="total_row[]" id="total_row1" data-srno="1" class="form-control text-right total_row" required disabled>
          </td>

        </tr>
      </tbody>
    </table>

    <div class="col-12">
      <a href="#" role="button" class="btn btn-success btn-sm font-weight-bold" title="Add Row" id="add_row">+</a>
    </div>

    <div class="row my-2">
      <div class="col-3">
        <h4 class="text-primary">Supplied provider</h4>
      </div>
      <div class="col-9">
        <h5 class="supplied" id="supplied"></h5>
      </div>
    </div>

    <div class="row">
      <div class="col">
        <h5 class="text-success">Cheap: <span class="cheap"></span></h5>
      </div>
      <div class="col">
        <h5 class="text-success">Expensive: <span class="expensive"></span></h5>
      </div>
    </div>

    <div class="row">
      <div class="col-6 text-right">
        <h3>Total</h3>
      </div>

      <div class="col-6 text-right">
        <h3 class="total-order">0.00</h3>
        <input type="hidden" name="total" id="total">
      </div>
    </div>

    <div class="col-12 text-center">
      <input type="submit" value="Save" class="btn btn-secondary">
    </div>
  </form>
</div>