<?php defined('BASEPATH') or exit('No direct access'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php echo TITLE ?></title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-primary">
    <a class=" navbar-brand" href="/" style="color: #fff !important;"><?php echo TITLE ?></a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="/product">Products <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/provider">Providers</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/order">New Order</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="container">
    <div class="jumbotron m-3">
      <?php !empty($show_order_list) ? require ROOT . "app/views/Order/order_list.php" : "" ?>
      <?php !empty($form_order) ? require ROOT . "app/views/Order/form_order.php" : "" ?>
      <?php !empty($order_detail) ? require ROOT . "app/views/order/order_detail.php" : "" ?>
    </div>
  </div>

  <script>
    $(document).ready(function() {
      var count = 1;

      function calcRow($inRow) {
        let quantity, price, inRow;
        let totalRow;
        let percentage = '',
          quantityFrom = '';

        if ($('#percentage').val() != '') {
          percentage = parseFloat($('#percentage').val()) / 100;
        }

        if ($('#quantityFrom').val() != '') {
          quantityFrom = Number($('#quantityFrom').val());
        }

        price = $(`#price_row${$inRow}`).val();

        if (!($(`#quantity_row${$inRow}`).val())) {
          $(`#quantity_row${$inRow}`).val(Number(1));
        }

        quantity = $(`#quantity_row${$inRow}`).val();

        if (price && quantity) {
          if (quantity >= quantityFrom) {
            totalRow = parseFloat(price) * parseFloat(quantity);
            totalRow = totalRow - (percentage * totalRow);
          } else {
            totalRow = parseFloat(price) * parseFloat(quantity);
          }

          $(`#total_row${$inRow}`).val(totalRow.toFixed(2));
        } else {
          $(`#total_row${$inRow}`).val('');
        }

        let providersId = [];
        let providersName = [];

        for (let i = 1; i <= count; i++) {
          if (!providersId.includes(Number($(`#providers_row${i}`).children('option:selected').val()))) {
            providersId.push(Number($(`#providers_row${i}`).children('option:selected').val()));

            providersName[$(`#providers_row${i}`).children('option:selected').text()] = parseFloat($(`#quantity_row${i}`).val());

          } else {
            providersName[$(`#providers_row${i}`).children('option:selected').text()] = parseFloat(providersName[$(`#providers_row${i}`).children('option:selected').text()]) + parseFloat($(`#quantity_row${i}`).val());
          }
        }

        $('.supplied').text('');

        for (let [key, value] of Object.entries(providersName)) {
          $('.supplied').append(
            ` ${key}: ${value} `
          );
        }
      }

      function totalOrder() {
        let totalOrd = 0;
        for (i = 1; i <= count; i++) {
          totalOrd = parseFloat(totalOrd) + parseFloat($(`#total_row${i}`).val());
        }

        $('.total-order').text(totalOrd.toFixed(2));
        $('#total').val(totalOrd.toFixed(2));
      }

      $(document).on('click', '#add_row', function(e) {
        e.preventDefault();
        var optionProducts = '';
        count++;

        $('#totalRows').val(count);

        $.ajax({
          type: "GET",
          dataType: 'json',
          url: '/product/dataAjax',
          success: function(data) {
            optionProducts = data.products.map(option => {
              return `<option value="${option.id}">${option.name}</option>`
            });

            optionProviders = data.providers.map(option => {
              return `<option value="${option.id}">${option.name}</option>`
            });

            let html_code = '';

            html_code += `<tr id="row_id_${count}">`;

            html_code += `<td><select name="product[]" id="product_row${count}" data-srno="${count}" class="form-control" required><option value="">Seleccione</option>${optionProducts}</select></td>`;

            html_code += `<td><select name="providers[]" id="providers_row${count}" class="form-control" data-srno="${count}" required><option value="">Seleccione</option>${optionProviders}</select></td>`;

            html_code += `<td><input type="text" name="price[]" id="price_row${count}" data-srno="${count}" class="form-control text-right price" required></td>`;

            html_code += `<td><input type="number" min="1" name="quantity[]" id="quantity_row${count}" data-srno="${count}" class="form-control text-right quantity" required></td>`;

            html_code += `<td><input type="text" min="1" name="total_row[]" id="total_row${count}" data-srno="${count}" class="form-control text-right total_row" required disabled></td>`;

            html_code += `</tr>`;

            $('#order-table').append(html_code);
          }
        });

      });

      $(document).on('keypress keyup blur', '.price', (function(e) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));

        if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
          e.preventDefault();
        }
      }));

      $(document).on('keyup', '.price', (function(e) {
        $inRow = $(this).data('srno');

        calcRow($inRow);

        totalOrder();
      }));

      $(document).on('keyup change', '.quantity', function() {
        $inRow = $(this).data('srno');

        calcRow($inRow);

        totalOrder();

        const prices = [];
        $('input[name="total_row[]"]').each(function() {
          let price = parseFloat($(this).val());
          prices.push(price)
        });

        prices.sort(function(a, b) {
          return a - b
        });

        $('.cheap').text(parseFloat(prices[0]).toFixed(2));
        $('.expensive').text(parseFloat(prices[prices.length - 1]).toFixed(2));
      });
    });
  </script>
</body>

</html>