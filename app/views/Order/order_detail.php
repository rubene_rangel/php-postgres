<div class="row">
  <div class="col-12">
    <table class="table table-stripped">
      <thead>
        <tr>
          <th colspan="5" class="text-center">
            <h3>Order Details</h3>
          </th>
        </tr>
        <tr>
          <th>ID</th>
          <th>Product Name</th>
          <th>Provider Name</th>
          <th>Price</th>
          <th>Quantity</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($orderDetails as $detail) : ?>
          <tr>
            <td><?php echo $detail['id_order'] ?></td>
            <td><?php echo $detail['name_product'] ?></td>
            <td><?php echo $detail['name_provider'] ?></td>
            <td><?php echo $detail['price'] ?></td>
            <td><?php echo $detail['quantity'] ?></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>