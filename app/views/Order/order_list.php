<div class="col-12 text-center">
  <h2>List Orders</h2>
</div>
<div class="col-12">
  <table class="table">
    <thead>
      <tr>
        <th>Order Nº</th>
        <th>Order Date</th>
        <th>Order Total</th>
        <th class="text-right"> <a href="/order/addOrder" role="button" class="btn btn-success btn-sm text-right">Create</a></th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($orders as $order) : ?>
        <tr>
          <td><?php echo $order['id'] ?></td>
          <td><?php echo $order['created_at'] ?></td>
          <td><?php echo $order['total'] ?></td>
          <td>
            <a href="/order/show/<?php echo $order['id']; ?>" class="btn btn-sm btn-primary">Details</a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>