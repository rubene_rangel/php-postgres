<?php defined('BASEPATH') or exit('No direct access'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php echo TITLE ?></title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-primary">
    <a class=" navbar-brand" href="/" style="color: #fff !important;"><?php echo TITLE ?></a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="/product">Products <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/provider">Providers</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/order">New Order</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="container">
    <div class="jumbotron m-3">
      <div class="col-12 text-center">
        <h2>Providers</h2>
      </div>
      <div class="col-md-6">
        <h2>Add providers</h2>
        <form action="/provider/addProvider" method="post">

          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control" placeholder="Name" required>
          </div>

          <div class="row">
            <?php
            !empty($message) ? print("<div class='alert alert-warning'>$message</div>") : "";
            ?>
          </div>

          <button type="submit" class="btn btn-primary">Add</button>
        </form>
      </div>
      <div class="col-12">
        <table class="table">
          <thead>
            <tr>
              <td colspan="2" class="text-center">
                <h2>List Providers</h2>
              </td>
            </tr>
            <tr>
              <th>ID</th>
              <th>Name</th>

            </tr>
          </thead>
          <tbody>
            <?php foreach ($providers as $provider) : ?>
              <tr>
                <td><?php echo $provider['id']; ?></td>
                <td><?php echo $provider['name']; ?></td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</body>

</html>