CREATE OR REPLACE FUNCTION public.create_order(total double precision, OUT ord_id integer)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
BEGIN
	INSERT INTO 
		orders(total)
	VALUES(total)
	RETURNING id
	INTO ord_id;
END;
$function$
