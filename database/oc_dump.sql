--
-- PostgreSQL database dump
--

-- Dumped from database version 10.12
-- Dumped by pg_dump version 10.12

-- Started on 2020-03-09 03:01:39

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2839 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 205 (class 1255 OID 16820)
-- Name: create_order(double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.create_order(total double precision, OUT ord_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO 
		orders(total)
	VALUES(total)
	RETURNING id
	INTO ord_id;
END;
$$;


ALTER FUNCTION public.create_order(total double precision, OUT ord_id integer) OWNER TO postgres;

--
-- TOC entry 206 (class 1255 OID 16824)
-- Name: details_order(integer, integer, integer, double precision, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.details_order(id_order integer, id_product integer, id_provider integer, price double precision, quantity integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	INSERT INTO 
		order_details(id_order, id_product, id_provider, price, quantity)
		VALUES(id_order, id_product, id_provider, price, quantity);
		
END;
$$;


ALTER FUNCTION public.details_order(id_order integer, id_product integer, id_provider integer, price double precision, quantity integer) OWNER TO postgres;

--
-- TOC entry 220 (class 1255 OID 16841)
-- Name: get_order_details(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_order_details(id integer) RETURNS TABLE(id_order integer, name_product text, name_provider text, price double precision, quantity integer)
    LANGUAGE plpgsql
    AS $_$BEGIN
	return query
	SELECT
		od.id_order
		,p.name
		,prov.name
		,od.price
		,od.quantity
	FROM
		order_details od
	INNER JOIN
		products p
	ON
		od.id_product = p.id
	INNER JOIN
		providers prov
	ON
		od.id_provider = prov.id
	WHERE
		od.id_order = $1;
END;
$_$;


ALTER FUNCTION public.get_order_details(id integer) OWNER TO postgres;

--
-- TOC entry 207 (class 1255 OID 16816)
-- Name: get_orders(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_orders() RETURNS TABLE(id integer, total double precision, created_at timestamp with time zone)
    LANGUAGE plpgsql
    AS $$begin
	return query
	select 
		o.id,
		o.total,
		o.created_at
	FROM
		orders o
	ORDER BY
		created_at DESC;
end; $$;


ALTER FUNCTION public.get_orders() OWNER TO postgres;

--
-- TOC entry 203 (class 1255 OID 16790)
-- Name: get_products(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_products() RETURNS TABLE(id integer, name text, price double precision)
    LANGUAGE plpgsql
    AS $$BEGIN
	return query
	SElECT 
		p.id,
		p.name,
		p.price
	FROM
		products p;
END; $$;


ALTER FUNCTION public.get_products() OWNER TO postgres;

--
-- TOC entry 204 (class 1255 OID 16792)
-- Name: get_providers(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_providers() RETURNS TABLE(id integer, name text)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT
		pro.id,
		pro.name
	FROM
		providers pro;
END; $$;


ALTER FUNCTION public.get_providers() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 199 (class 1259 OID 16748)
-- Name: providers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.providers (
    id integer NOT NULL,
    name text
);


ALTER TABLE public.providers OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16746)
-- Name:  providers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public." providers_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public." providers_id_seq" OWNER TO postgres;

--
-- TOC entry 2840 (class 0 OID 0)
-- Dependencies: 198
-- Name:  providers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public." providers_id_seq" OWNED BY public.providers.id;


--
-- TOC entry 202 (class 1259 OID 16817)
-- Name: order_details; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.order_details (
    id_order integer NOT NULL,
    id_product integer NOT NULL,
    id_provider integer NOT NULL,
    price double precision NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE public.order_details OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16803)
-- Name: orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orders (
    id integer NOT NULL,
    total double precision NOT NULL,
    created_at timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.orders OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16801)
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orders_id_seq OWNER TO postgres;

--
-- TOC entry 2841 (class 0 OID 0)
-- Dependencies: 200
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;


--
-- TOC entry 197 (class 1259 OID 16737)
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products (
    id integer NOT NULL,
    name text,
    price double precision
);


ALTER TABLE public.products OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16735)
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.products_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO postgres;

--
-- TOC entry 2842 (class 0 OID 0)
-- Dependencies: 196
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- TOC entry 2696 (class 2604 OID 16806)
-- Name: orders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


--
-- TOC entry 2694 (class 2604 OID 16740)
-- Name: products id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- TOC entry 2695 (class 2604 OID 16751)
-- Name: providers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.providers ALTER COLUMN id SET DEFAULT nextval('public." providers_id_seq"'::regclass);


--
-- TOC entry 2831 (class 0 OID 16817)
-- Dependencies: 202
-- Data for Name: order_details; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.order_details (id_order, id_product, id_provider, price, quantity) FROM stdin;
45	7	3	300.5	20
45	7	4	290.5	15
45	8	3	234.5	12
45	9	4	256.5	50
46	7	3	300.5	20
46	7	4	290.5	15
46	8	3	234.5	12
46	9	4	256.5	50
47	7	3	300.5	20
\.


--
-- TOC entry 2830 (class 0 OID 16803)
-- Dependencies: 201
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orders (id, total, created_at) FROM stdin;
45	26006.5	2020-03-09 01:58:32.306191-04
46	20356	2020-03-09 02:00:06.504596-04
47	6010	2020-03-09 02:02:49.647932-04
\.


--
-- TOC entry 2826 (class 0 OID 16737)
-- Dependencies: 197
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.products (id, name, price) FROM stdin;
7	PR001	234.5
8	PR002	234.5
9	PR003	256.5
\.


--
-- TOC entry 2828 (class 0 OID 16748)
-- Dependencies: 199
-- Data for Name: providers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.providers (id, name) FROM stdin;
3	PROV0001
4	PROV0002
\.


--
-- TOC entry 2843 (class 0 OID 0)
-- Dependencies: 198
-- Name:  providers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public." providers_id_seq"', 4, true);


--
-- TOC entry 2844 (class 0 OID 0)
-- Dependencies: 200
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orders_id_seq', 47, true);


--
-- TOC entry 2845 (class 0 OID 0)
-- Dependencies: 196
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.products_id_seq', 9, true);


--
-- TOC entry 2701 (class 2606 OID 16756)
-- Name: providers  providers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.providers
    ADD CONSTRAINT " providers_pkey" PRIMARY KEY (id);


--
-- TOC entry 2703 (class 2606 OID 16808)
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- TOC entry 2699 (class 2606 OID 16745)
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


-- Completed on 2020-03-09 03:01:47

--
-- PostgreSQL database dump complete
--

