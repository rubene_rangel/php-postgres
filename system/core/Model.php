<?php
defined('BASEPATH') or exit('No direct access');

/**
 * undocumented class
 */
class Model
{
  protected $db;

  public function __construct()
  {
    $this->db = new PDO(
      'pgsql:host=' . HOST . ';' .
        'port=' . PORT . ';' .
        'dbname=' . DB_NAME . ';' .
        'user=' . USER . ';' .
        'password=' . PASSWORD . ';'
    );

    $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  public function __destruct()
  {
    $this->db = null;
  }
}
